import {createAction} from '@ngrx/store';

export const loadCollection = createAction(
  '[Contacts Page] Load Collection'
);
