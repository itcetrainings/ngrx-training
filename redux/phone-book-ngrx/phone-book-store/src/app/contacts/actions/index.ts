import * as CollectionApiActions from './collection-api.actions';
import * as ContactsPageActions from './contacts-page.actions';

export {
  CollectionApiActions,
  ContactsPageActions
};
