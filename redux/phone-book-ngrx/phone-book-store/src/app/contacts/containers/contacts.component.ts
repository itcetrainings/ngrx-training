import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';

import * as fromRoot from '../../reducers/index';

import { Contact } from '../models/contact.model';
import {ContactsPageActions} from '../actions';

@Component({
  selector: 'contacts',
  template: `
    <div *ngIf="(loaded$ | async) === true">
        <contacts-list [contacts]="contacts$ | async"></contacts-list>

        <a id="add" class="text-danger" [routerLink]="['/contacts', -1]"><span class="glyphicon glyphicon-plus"></span>Add</a>

        <router-outlet></router-outlet>
    </div>
    <div *ngIf="(loading$ | async) === true">
        Loading...
    </div>
  `
})
export class ContactsComponent implements OnInit {
  contacts$: Observable<Contact[]>;
  loading$: Observable<boolean>;
  loaded$: Observable<boolean>;

  constructor(
    private store: Store<fromRoot.State>
  ) {

    this.contacts$ = store.pipe(
      select(fromRoot.getContactEntitiesState)
    );

    this.loading$ = store.pipe(
      select(fromRoot.getContactsLoading)
    );

    this.loaded$ = store.pipe(
      select(fromRoot.getContactsLoaded)
    );
  }

  ngOnInit(): void {
    this.store.dispatch(ContactsPageActions.loadCollection());
  }

}
