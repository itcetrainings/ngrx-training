import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

import {Group} from '../models/group.model';
import {ContactsService} from '../services/contact.service';
import {Contact} from '../models/contact.model';

@Component({
  selector: 'contact-details',
  template: `
      <contact-card *ngIf="contact && !showEdit">
        <div id="contactsDetailsContainer" *ngIf="contact">
            <ng-container *ngIf="!showEdit">
                <label>First Name: </label><b>{{contact.firstName}}</b><br/>
                <label>Last Name: </label><b>{{contact.lastName}}</b><br/>
                <label>Email: </label><b>{{contact.email}}</b><br/>
                <label>Groups: </label>
                <span *ngFor="let group of contact.groups" class="label display-label inline-block label-success">
                        <span>{{group.name}}</span>
                    </span>
                <div>
                    <a class="text-danger" (click)="showEdit=true">
                        <span class="glyphicon glyphicon-edit"></span>
                        Edit
                    </a>
                </div>
            </ng-container>
        </div>
      </contact-card>
      <contact-form [(showEdit)]="showEdit" [contact]="contact" (contactSubmit)="onSubmit($event)"></contact-form>
  `,
  styles: ['.alert {margin-left: 104px;}']
})
export class ContactDetailsComponent implements OnInit, OnDestroy {
  contact: Contact;
  unsubscribe$ = new Subject<void>();
  groups: Group[];
  showEdit: boolean;

  constructor(
    private contactsService: ContactsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((params) => {
        if (params['id']) {
          if (params['id'] !== '-1') {
            this.contactsService.getById(params['id']).subscribe(contact => {
              this.contact = contact as Contact;
              this.showEdit = false;
            });
          } else {
            this.contact = {firstName: '', lastName: '', email: '', groups: []} as Contact;
            this.showEdit = true;
          }
        } else {
          this.contact = null;
        }
      });
  }

  onSubmit(contact: Contact) {
    if (!contact.id) {
      this.contactsService.add(contact)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(newContact => {
          this.resetFormAndNavigate(newContact as Contact);
        });
    } else {
      this.contactsService.update(contact)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(updatedContact => {
          this.resetFormAndNavigate(updatedContact as Contact);
        });
    }
  }

  resetFormAndNavigate(contact: Contact) {
    this.contact = contact;
    this.router.navigate(['/contacts'])
      .then(() => {
        this.router.navigate(['/contacts', contact.id]);
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
