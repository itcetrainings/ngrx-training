import {createReducer, on} from '@ngrx/store';

import { Contact } from '../models/contact.model';
import {CollectionApiActions, ContactsPageActions} from '../actions';

export interface State {
  loading: boolean;
  loaded: boolean;
  data: Contact[];
}

export const initialState: State = {
  data: [],
  loading: false,
  loaded: false
};

export const contactsFeatureKey = 'contacts';

export const reducer = createReducer(
  initialState,
  on(ContactsPageActions.loadCollection, state => ({
    ...state,
    loading: true,
    loaded: false
  })),
  // @ts-ignore
  on(CollectionApiActions.loadContactsSuccess,  (state, { contacts }) => ({
    loaded: true,
    loading: false,
    data: contacts
  }))
);


export const getLoaded = (state: State) => state.loaded;

export const getLoading = (state: State) => state.loading;

export const getData = (state: State) => state.data;
