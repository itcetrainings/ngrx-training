import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  Action,
  MetaReducer, ActionReducer
} from '@ngrx/store';
import {InjectionToken} from '@angular/core';

import { environment } from '../../environments/environment';

import * as fromContacts from '../contacts/reducers/contacts.reducer';

export interface State {
  [fromContacts.contactsFeatureKey]: fromContacts.State;
}

//

export const reducers = new InjectionToken<
  ActionReducerMap<State, Action>
  >('Root reducers token', {
  factory: () => ({
    [fromContacts.contactsFeatureKey]: fromContacts.reducer,
  }),
});


// console.log all actions
export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return (state, action) => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();

    return result;
  };
}

export const metaReducers: MetaReducer<State>[] = !environment.production ? [logger] : [];

//

export const getContactsState = createFeatureSelector<State, fromContacts.State>(
  fromContacts.contactsFeatureKey
);

//

export const getContactEntitiesState = createSelector(
  getContactsState,
  fromContacts.getData
);

export const getContactsLoading = createSelector(
  getContactsState,
  fromContacts.getLoading
);

export const getContactsLoaded = createSelector(
  getContactsState,
  fromContacts.getLoaded
);
