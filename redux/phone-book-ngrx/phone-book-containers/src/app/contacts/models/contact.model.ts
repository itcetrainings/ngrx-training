import {Group} from './group.model';

export interface Contact {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  groups: Group[];
}
