import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ContactsComponent} from './contacts/containers/contacts.component';
import {GroupsListComponent} from './contacts/components/groups-list.component';
import {ContactDetailsComponent} from './contacts/containers/contact-details.component';
import {AboutComponent} from './about/containers/about.component';
import {ContactsListComponent} from './contacts/components/contacts-list.component';
import {ContactCardComponent} from './contacts/components/contact-card.component';
import {ContactsService} from './contacts/services/contact.service';
import {GroupsService} from './contacts/services/groups.service';
import {ContactFormComponent} from './contacts/components/contact-form.component';

@NgModule({
  imports: [BrowserModule, FormsModule, AppRoutingModule, ReactiveFormsModule, HttpClientModule],
  declarations: [
    AppComponent,
    ContactsComponent,
    ContactsListComponent,
    ContactDetailsComponent,
    GroupsListComponent,
    AboutComponent,
    ContactCardComponent,
    ContactFormComponent
  ],
  bootstrap: [AppComponent],
  providers: [ContactsService, GroupsService]
})
export class AppModule {
}
