import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {ContactsComponent} from './contacts/contacts.component';
import {ContactsListComponent} from './contacts/contacts-list.component';
import {ContactDetailsComponent} from './contacts/contact-details.component';
import {ContactsService} from './contacts/contact.service';
import {GroupsService} from './contacts/groups.service';
import {AboutComponent} from './about/about.component';
import {ContactCardComponent} from './contacts/contact-card.component';
import {GroupsListComponent} from './contacts/groups-list.component';

import {AppRoutingModule} from './app-routing.module';

@NgModule({
  imports: [BrowserModule, FormsModule, AppRoutingModule, ReactiveFormsModule, HttpClientModule],
  declarations: [
    AppComponent,
    ContactsComponent, ContactsListComponent,
    ContactDetailsComponent,
    GroupsListComponent,
    AboutComponent,
    ContactCardComponent],
  bootstrap: [AppComponent],
  providers: [ContactsService, GroupsService]
})
export class AppModule {
}
