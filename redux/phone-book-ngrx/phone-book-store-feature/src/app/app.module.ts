import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {StoreModule} from '@ngrx/store';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import { metaReducers} from './reducers';

import { environment } from '../environments/environment';
import {ContactsModule} from './contacts/contacts.module';
import {AboutComponent} from './about/containers/about.component';
import {EffectsModule} from '@ngrx/effects';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule, AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ContactsModule,
    StoreModule.forRoot([], {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      name: 'Angular Training App',
      logOnly: environment.production
    })]
  ,
  declarations: [
    AppComponent,
    AboutComponent
  ],
  bootstrap: [AppComponent],
  providers: []
})
export class AppModule {
}
