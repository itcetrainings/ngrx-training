import {createAction, props} from '@ngrx/store';

export const loadCollection = createAction(
  '[Contacts Page] Load Collection'
);

export const selectContact = createAction(
  '[Contacts Page] Select Contact',
  props<{ id: number }>()
);
