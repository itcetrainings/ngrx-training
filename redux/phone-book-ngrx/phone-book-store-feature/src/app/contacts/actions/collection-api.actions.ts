import {createAction, props} from '@ngrx/store';

import {Contact} from '../models/contact.model';

export const loadContactsSuccess = createAction(
  '[Contacts API] Load Contacts Success',
  props<{ contacts: Contact[] }>()
);

export const loadContactsFailure = createAction(
  '[Contacts API] Load Contacts Failure',
  props<{ error: any }>()
);

