import {Action, combineReducers, createFeatureSelector, createSelector} from '@ngrx/store';

import * as fromRoot from '../../reducers/index';
import * as fromContacts from './contacts.reducer';

export const contactsFeatureKey = 'contacts';

export interface ContactsState {
  [fromContacts.contactsFeatureKey]: fromContacts.State;
}

export interface State extends fromRoot.State {
  [contactsFeatureKey]: ContactsState;
}

export function reducers(state: ContactsState | undefined, action: Action) {
  return combineReducers({
    [fromContacts.contactsFeatureKey]: fromContacts.reducer,
  })(state, action);
}

//

export const getContactsState = createFeatureSelector<State, ContactsState>(
  contactsFeatureKey
);


//

export const getContactEntitiesState = createSelector(
  getContactsState,
  state => state.contacts
);

export const getContacts = createSelector(
  getContactEntitiesState,
  fromContacts.getData
);

export const getContactsLoading = createSelector(
  getContactEntitiesState,
  fromContacts.getLoading
);

export const getContactsLoaded = createSelector(
  getContactEntitiesState,
  fromContacts.getLoaded
);
