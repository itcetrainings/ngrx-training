import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import {ContactsRoutingModule} from './contacts-routing.module';
import {DialogService} from '../dialog.service';
import {ContactsComponent} from './containers/contacts.component';
import {ContactsListComponent} from './components/contacts-list.component';
import {ContactDetailsComponent} from './containers/contact-details.component';
import {ContactsService} from './services/contact.service';
import {GroupsListComponent} from './components/groups-list.component';
import {ContactCardComponent} from './components/contact-card.component';
import {ContactFormComponent} from './components/contact-form.component';
import {ContactsEffects} from './effects/contacts.effects';

import * as fromContacts from './reducers';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ContactsRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forFeature(fromContacts.contactsFeatureKey, fromContacts.reducers),
    EffectsModule.forFeature([ContactsEffects]),
  ],
  declarations: [
    ContactsComponent,
    ContactsListComponent,
    ContactDetailsComponent,
    GroupsListComponent,
    ContactCardComponent,
    ContactFormComponent
  ],
  providers: [ContactsService, DialogService]
})
export class ContactsModule {
}
