import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryAppService implements InMemoryDbService {
  createDb() {
    const contacts = [
        {
          id: 1,
          'firstName': 'Gordon',
          'lastName': 'Freeman',
          'email': 'freeman@blackmesa.com'
        },
        {
          'id': 2,
          'firstName': 'Alyx',
          'lastName': 'Vance',
          'email': 'alyx@resitance.com'
        },
        {
          'id': 3,
          'firstName': 'Wallace',
          'lastName': 'Breen',
          'email': 'breen@blackmesa.com'
        },
        {
          'id': 4,
          'firstName': 'Barney',
          'lastName': 'Calhoun',
          'email': 'calhoun@blackmesa.com'
        },
        {
          'id': 5,
          'firstName': 'Eli',
          'lastName': 'Vance',
          'email': 'vance@blackmesa.com'
        }
      ];

    const groups = [
      {
        'id': 1,
        'name': 'Family'
      },
      {
        'id': 2,
        'name': 'Friends'
      },
      {
        'id': 3,
        'name': 'Business'
      },
      {
        'id': 4,
        'name': 'Acquaintances'
      },
      {
        'id': 5,
        'name': 'Services'
      }
    ];
    return {contacts, groups};
  }
}
