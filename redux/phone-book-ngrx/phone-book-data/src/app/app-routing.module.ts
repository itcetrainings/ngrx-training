import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


import {CanDeactivateGuard} from './contacts/guards/can-deactivate-guard';
import {ContactsComponent} from './contacts/containers/contacts.component';
import {ContactDetailsComponent} from './contacts/containers/contact-details.component';
import {AboutComponent} from './about/containers/about.component';

const routes: Routes = [
  {
    path: 'contacts',
    component: ContactsComponent,
    children: [
      {path: ':id', component: ContactDetailsComponent, canDeactivate: [CanDeactivateGuard]},
    ]
  },
  {path: 'about', component: AboutComponent},
  {path: '', redirectTo: '/contacts', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule],
  providers: [CanDeactivateGuard]
})
export class AppRoutingModule {
}
