import { NgModule } from '@angular/core';

import {
  DefaultDataServiceConfig, EntityDataModule,
} from '@ngrx/data';
import { entityMetadata } from './entity-metadata';

const defaultDataServiceConfig: DefaultDataServiceConfig = {
  root: 'api',
  entityHttpResourceUrls: {
    Contact: {
      entityResourceUrl: 'api/contacts',
      collectionResourceUrl: 'api/contacts'
    },
    Group: {
      entityResourceUrl: 'api/groups',
      collectionResourceUrl: 'api/groups'
    }
  },
  timeout: 3000
};

@NgModule({
  imports: [
    EntityDataModule.forRoot({
      entityMetadata: entityMetadata
    })
  ],
  providers: [
    { provide: DefaultDataServiceConfig, useValue: defaultDataServiceConfig },
  ]
})
export class EntityStoreModule {}
