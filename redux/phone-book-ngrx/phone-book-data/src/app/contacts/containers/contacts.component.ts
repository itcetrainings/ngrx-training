import {Component, OnInit} from '@angular/core';
import {select} from '@ngrx/store';
import {Observable} from 'rxjs';
import {EntityCollectionService, EntityCollectionServiceFactory} from '@ngrx/data';

import {Contact} from '../models/contact.model';


@Component({
  selector: 'contacts',
  template: `
      <div *ngIf="(loaded$ | async) === true">
          <contacts-list
            [selectedContact]="selectedContact$ | async"
            [contacts]="contacts$ | async"
            (contactRemove)="onContactRemove($event)">
          </contacts-list>
          
          {{ contacts$ | async}}

          <a id="add" class="text-danger" [routerLink]="['/contacts', -1]"><span class="glyphicon glyphicon-plus"></span>Add</a>

          <router-outlet></router-outlet>
      </div>
      <div *ngIf="(loading$ | async) === true">
          Loading...
      </div>
  `
})
export class ContactsComponent implements OnInit {
  contacts$: Observable<Contact[]>;
  selectedContact$: Observable<Contact>;
  loading$: Observable<boolean>;
  loaded$: Observable<boolean>;

  contactsService: EntityCollectionService<Contact>;

  constructor(
    private entityCollectionServiceFactory: EntityCollectionServiceFactory
  ) {
    this.contactsService = this.entityCollectionServiceFactory.create<Contact>('Contact');

  }

  ngOnInit(): void {
    this.contactsService.getAll();

    this.loading$ = this.contactsService.loading$;

    // this.selectedContact$ = this.contactsService.selectors$.selectId

    this.contacts$ = this.contactsService.entities$;
  }

  onContactRemove(contact: Contact) {
    this.contactsService.delete(contact.id);
  }

}
