import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

import {Group} from '../models/group.model';

@Component({
  selector: 'groups-display',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
        <label><strong>Selected:</strong></label>
        <div *ngFor="let group of groups" class="label checkbox-inline label-success">
            <span *ngIf="group">{{group.name}}</span>
        </div>
  `
})
export class GroupsDisplayComponent {
  @Input() groups: Group[] = [];
}
