import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {map, take, takeUntil} from 'rxjs/operators';
import {fromPromise} from 'rxjs/internal-compatibility';
import {Subject} from 'rxjs';

import {DialogService} from '../../dialog.service';
import {Group} from '../models/group.model';
import {Contact} from '../models/contact.model';


@Component({
  selector: 'contact-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
      <contact-card *ngIf="contact && showEdit">
          <div id="contactsDetailsContainer">
              <form [formGroup]="contactForm" novalidate>
                  <label for="firstName">First Name: </label>
                  <input id="firstName" name="firstName" formControlName="firstName" required><br/>
                  <div class="alert alert-danger" role="alert"
                       *ngIf="contactForm.controls.firstName && !contactForm.controls.firstName.pristine &&
               !contactForm.controls.firstName.valid">
                      First name is required
                  </div>

                  <label for="lastName">Last Name: </label>
                  <input id="lastName" name="lastName" formControlName="lastName"
                         required><br/>
                  <div class="alert alert-danger" role="alert"
                       *ngIf="contactForm.controls.lastName && !contactForm.controls.lastName.pristine && !contactForm.controls.lastName.valid">
                      Last name is required
                  </div>

                  <label for="email">Email: </label>
                  <input id="email" name="email" formControlName="email">
                  <div class="alert alert-danger" role="alert"
                       *ngIf="contactForm.controls.email && !contactForm.controls.email.valid">Email is invalid
                  </div>
                  <div class="form-group">
                      <label>Groups: </label>
                      <groups-list formControlName="groups" [availableGroups]="groups"></groups-list>
                  </div>
                  <ng-content select="contact-info"></ng-content>
                  <input type="submit" class="btn btn-danger" (click)="onSubmit()" value="{{ !contact.id ? 'Add' : 'Save' }}"
                         [disabled]="contactForm.invalid || contactForm.pristine"/>
                  <a href="#" class="text-danger" (click)="onCancel()">Cancel</a>
              </form>
          </div>
      </contact-card>
  `,
})
export class ContactFormComponent implements OnChanges, OnDestroy {
  @Input() contact: Contact;
  @Input() groups: Group[];
  @Input() showEdit: boolean;
  @Output() showEditChange = new EventEmitter<boolean>();
  @Output() contactSubmit = new EventEmitter<Contact>();
  @Output() selectedGroupsChange = new EventEmitter<number[]>();

  unsubscribe$ = new Subject<void>();
  contactForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: DialogService
  ) {
    this.contactForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.email],
      groups: [[]]
    });

    this.contactForm.get('groups').valueChanges
      .pipe(
        takeUntil(this.unsubscribe$),
        map((groups: Group[]) => groups.map((group) => group.id))
      ).subscribe(values => this.selectedGroupsChange.emit(values));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.contact && changes.contact.currentValue) {
      this.resetForm();
    }
  }

  onSubmit() {
    if (!this.contactForm.valid) {
      return;
    }
    this.showEdit = false;

    const contact: Contact = this.contactForm.value;
    contact.id = this.contact.id;

    this.contactSubmit.emit(contact);
  }

  onCancel() {
    this.showEditChange.emit(false);
  }

  canDeactivate() {
    if (!this.contactForm.dirty) {
      return true;
    }
    const dialogResult = fromPromise(this.dialogService.confirm('Discard changes?'));

    dialogResult
      .pipe(take(1))
      .subscribe(
        (result) => {
          if (!result) {
            this.showEditChange.emit(true);
          }
        });

    return dialogResult;
  }

  resetForm() {
    this.contactForm.reset({
      firstName: this.contact.firstName,
      lastName: this.contact.lastName,
      email: this.contact.email,
      groups: this.contact.groups
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
