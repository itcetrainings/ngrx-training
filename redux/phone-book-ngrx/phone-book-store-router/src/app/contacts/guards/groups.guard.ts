import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, filter, switchMap, take, tap} from 'rxjs/operators';
import {Store} from '@ngrx/store';

import * as fromContacts from '../reducers/index';

import {GroupActions} from '../actions';

@Injectable()
export class GroupsGuard implements CanActivate {
  constructor(
    private store: Store<fromContacts.State>
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return  this.checkStore().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromContacts.getGroupsLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(GroupActions.loadCollection());
        }
      }),
      filter(loaded => loaded),
      take(1)
    );
  }
}
