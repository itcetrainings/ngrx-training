import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs';

import {GroupsService} from '../services/groups.service';
import {GroupsApiActions, GroupActions} from '../actions';

@Injectable()
export class GroupsEffects {

  @Effect()
  loadGroups$ = this.actions$.pipe(
    ofType(GroupActions.loadCollection),
    switchMap(() => this.groupsService.getAll().pipe(
      map(groups => GroupsApiActions.loadGroupsSuccess({groups})),
      catchError(error =>
        of(GroupsApiActions.loadGroupsFailure(error))
      )
    ))
  );

  constructor(
    private actions$: Actions,
    private groupsService: GroupsService
  ) {
  }

}
