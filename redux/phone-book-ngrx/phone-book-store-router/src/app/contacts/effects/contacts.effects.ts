import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, filter, map, mergeMap, switchMap} from 'rxjs/operators';
import {ROUTER_NAVIGATION, RouterNavigationAction} from '@ngrx/router-store';
import {of} from 'rxjs';

import {ContactsApiActions, ContactDetailsPageActions, ContactsPageActions, ContactActions} from '../actions';
import {Contact} from '../models/contact.model';
import {ContactsService} from '../services/contact.service';

@Injectable()
export class ContactsEffects {
  loadCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactActions.loadCollection),
      switchMap(() =>
        this.contactsService.getAll().pipe(
          map((contacts: Contact[]) =>
            ContactsApiActions.loadContactsSuccess({contacts})
          ),
          catchError(error =>
            of(ContactsApiActions.loadContactsFailure({error}))
          )
        )
      )
    )
  );

  selectContact$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ROUTER_NAVIGATION),
      filter((action: RouterNavigationAction) => action.payload.routerState.url.startsWith('/contacts')),
      map((action: RouterNavigationAction<any>) => parseInt(action.payload.routerState.params['id'], 10)),
      mergeMap((id) =>
        of(ContactsPageActions.selectContact({id}))
      )
    )
  );

  loadContact$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactActions.loadContact),
      switchMap(({id}) =>
        this.contactsService.getById(id).pipe(
          map((contact) => ContactsApiActions.loadContactSuccess({contact})),
          catchError((error) =>
            of(ContactsApiActions.loadContactFailure({error}))
          )
        )
      )
    )
  );

  removeContactFromCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactsPageActions.removeContact),
      mergeMap(({contact}) =>
        this.contactsService.remove(contact.id).pipe(
          map(() => ContactsApiActions.removeContactSuccess({contact})),
          catchError(() =>
            of(ContactsApiActions.removeContactFailure({contact}))
          )
        )
      )
    )
  );

  addContactToCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactDetailsPageActions.addContact),
      mergeMap(({contact}) =>
        this.contactsService.add(contact).pipe(
          mergeMap(newContact =>
            [
              ContactsApiActions.addContactSuccess({contact: newContact}),
              ContactsPageActions.selectContact({id: newContact.id})
            ]),
          catchError(() =>
            of(ContactsApiActions.addContactFailure({contact}))
          )
        )
      )
    )
  );

  updateContactInCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactDetailsPageActions.updateContact),
      mergeMap(({contact}) =>
        this.contactsService.update(contact).pipe(
          map(updatedContact => ContactsApiActions.updateContactSuccess({contact: updatedContact})),
          catchError(error =>
            of(ContactsApiActions.updateContactFailure(error))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private contactsService: ContactsService
  ) {
  }
}
