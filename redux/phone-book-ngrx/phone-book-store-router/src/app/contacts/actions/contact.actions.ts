import { createAction, props } from '@ngrx/store';

import {Contact} from '../models/contact.model';

export const loadContact = createAction(
  '[Contact] Load Contact',
  props<{id: number }>()
);

export const loadCollection = createAction(
  '[Contact] Load Collection'
);



