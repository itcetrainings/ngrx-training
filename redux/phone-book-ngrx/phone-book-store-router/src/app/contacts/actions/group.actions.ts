import {createAction} from '@ngrx/store';

export const loadCollection = createAction(
  '[Group] Load Collection'
);
