import {createAction, props} from '@ngrx/store';

import {Contact} from '../models/contact.model';

export const addContact = createAction(
  '[Contact Details Page] Add Contact',
  props<{ contact: Contact }>()
);

export const updateContact = createAction(
  '[Contact Details Page] Update Contact',
  props<{ contact: Contact }>()
);

export const visualiseGroups = createAction(
  '[Contact Details Page] Visualise Groups',
  props<{ ids: number[] }>()
);


