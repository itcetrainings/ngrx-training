import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

import {Contact} from '../models/contact.model';

@Component({
  selector: 'contacts-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
      <ul>
          <li *ngFor="let contact of contacts" class="item" [class.active]="contact.id === selectedContact?.id">
              <a (click)="onSelect(contact)">{{contact.firstName}} {{contact.lastName | uppercase}}</a>
              <a (click)="remove(contact)" class="remove" title="Remove"><span
                      class="glyphicon glyphicon-remove-sign"></span></a>
          </li>
      </ul>
  `
})
export class ContactsListComponent {
  @Input() contacts: Contact[];
  @Input() selectedContact: Contact;
  @Output() contactRemove = new EventEmitter<Contact>();
  @Output() contactSelect = new EventEmitter<Contact>();

  remove(contact: Contact) {
    this.contactRemove.emit(contact);
  }

  onSelect(contact: Contact) {
    this.contactSelect.emit(contact);
  }

}
