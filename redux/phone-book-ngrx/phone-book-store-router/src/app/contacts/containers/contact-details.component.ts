import {Component, ComponentRef, OnDestroy, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {select, Store} from '@ngrx/store';;

import {Group} from '../models/group.model';
import {ContactsService} from '../services/contact.service';
import {Contact} from '../models/contact.model';

import {ContactDetailsPageActions} from '../actions';
import * as fromContacts from '../reducers';
import {ContactFormComponent} from '../components/contact-form.component';


@Component({
  selector: 'contact-details',
  template: `
      <contact-card *ngIf="contact && !showEdit">
          <div id="contactsDetailsContainer" *ngIf="contact">
              <ng-container *ngIf="!showEdit">
                  <label>First Name: </label><b>{{contact.firstName}}</b><br/>
                  <label>Last Name: </label><b>{{contact.lastName}}</b><br/>
                  <label>Email: </label><b>{{contact.email}}</b><br/>
                  <label>Groups: </label>
                  <span *ngFor="let group of contact.groups" class="label display-label inline-block label-success">
                        <span>{{group.name}}</span>
                    </span>
                  <div>
                      <a class="text-danger" (click)="showEdit=true">
                          <span class="glyphicon glyphicon-edit"></span>
                          Edit
                      </a>
                  </div>
              </ng-container>
          </div>
      </contact-card>
      <contact-form
        [showEdit]="showEdit"
        [groups]="groups$ | async"
        [contact]="contact"
        (contactSubmit)="onSubmit($event)"
        (selectedGroupsChange)="onSelectedGroupsChange($event)"
      >
        <contact-info>
            <div class="from-group">
                <groups-display [groups]="visualisedGroups$ | async"></groups-display>
            </div>
        </contact-info>
      </contact-form>
  `,
  styles: ['.alert {margin-left: 104px;}']
})
export class ContactDetailsComponent implements OnDestroy {
  @ViewChild(ContactFormComponent, {static: true}) contactFormComponent: ContactFormComponent;

  contact: Contact;
  unsubscribe$ = new Subject<void>();
  groups$: Observable<Group[]>;
  visualisedGroups$: Observable<Group[]>;
  showEdit: boolean;

  constructor(
    private contactsService: ContactsService,
    private router: Router,
    private store: Store<fromContacts.State>,
  ) {
    this.store.pipe(
      takeUntil(this.unsubscribe$),
      select(fromContacts.getSelectedContact),
    ).subscribe(contact => {
      this.contact = contact;
      this.showEdit = false;
    });

    this.store.pipe(
      takeUntil(this.unsubscribe$),
      select(fromContacts.isSelectedContactInCollection)
    ).subscribe(isContactInCollection => {
      if (isContactInCollection) {
        this.showEdit = false;
      } else {
        this.contact = {firstName: '', lastName: '', email: '', groups: []} as Contact;
        this.showEdit = true;
      }
    });

    this.groups$ = this.store.select(fromContacts.getGroupsCollection);
    this.visualisedGroups$ = this.store.select(fromContacts.getSelectedGroups);
  }

  onSubmit(contact: Contact) {
    if (!contact.id) {
      this.store.dispatch(ContactDetailsPageActions.addContact({contact}));
    } else {
      this.store.dispatch(ContactDetailsPageActions.updateContact({contact}));
    }
  }

  onSelectedGroupsChange(ids: number[]) {
    this.store.dispatch(ContactDetailsPageActions.visualiseGroups({ids}));
  }

  canDeactivate() {
    return this.contactFormComponent.canDeactivate();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
