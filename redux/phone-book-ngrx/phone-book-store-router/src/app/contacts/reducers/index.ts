import {Action, combineReducers, createFeatureSelector, createSelector} from '@ngrx/store';
import {Dictionary} from '@ngrx/entity';

import * as fromRoot from '../../reducers/index';
import * as fromContacts from './contact.reducer';
import * as fromGroups from './group.reducer';

import {Group} from '../models/group.model';

export const contactsFeatureKey = 'contacts';

export interface ContactsState {
  [fromContacts.contactsFeatureKey]: fromContacts.State;
  [fromGroups.groupsFeatureKey]: fromGroups.State;
}

export interface State extends fromRoot.State {
  [contactsFeatureKey]: ContactsState;
}

export function reducers(state: ContactsState | undefined, action: Action) {
  return combineReducers({
    [fromContacts.contactsFeatureKey]: fromContacts.reducer,
    [fromGroups.groupsFeatureKey]: fromGroups.reducer
  })(state, action);
}

//

export const getContactsState = createFeatureSelector<State, ContactsState>(
  contactsFeatureKey
);


//

export const getContactEntitiesState = createSelector(
  getContactsState,
  state => state.contacts
);

export const getGroupEntitiesState = createSelector(
  getContactsState,
  state => state.groups
);

//

export const getContactsLoading = createSelector(
  getContactEntitiesState,
  fromContacts.getLoading
);

export const getContactsLoaded = createSelector(
  getContactEntitiesState,
  fromContacts.getLoaded
);

export const getContactsSelectedId = createSelector(
  getContactEntitiesState,
  fromContacts.getSelectedId
);


export const getContactsCollection = createSelector(
  getContactEntitiesState,
  fromContacts.selectAll
);

export const getContactsDictionary = createSelector(
  getContactEntitiesState,
  fromContacts.selectEntities
);

export const getContactsIds = createSelector(
  getContactEntitiesState,
  fromContacts.selectIds
);

export const getSelectedContact = createSelector(
  getContactsDictionary,
  getContactsSelectedId,
  (entities, selectedId) => {
    return selectedId && entities[selectedId];
  }
);

export const isSelectedContactInCollection = createSelector(
  getContactsIds,
  getContactsSelectedId,
  (ids: number[], selected: number) => {
    return !!selected && ids.indexOf(selected) > -1;
  }
);

export const getGroupsCollection = createSelector(
  getGroupEntitiesState,
  fromGroups.selectAll
);

export const getGroupsDictionary = createSelector(
  getGroupEntitiesState,
  fromGroups.selectEntities
);

export const getSelectedGroupIds = createSelector(
  getGroupEntitiesState,
  fromGroups.getSelectedGroupIds
);

export const getGroupsLoading = createSelector(
  getGroupEntitiesState,
  fromGroups.getLoading
);

export const getGroupsLoaded = createSelector(
  getGroupEntitiesState,
  fromGroups.getLoaded
);

export const getSelectedGroups = createSelector(
  getSelectedGroupIds,
  getGroupsDictionary,
  (selectedIds: number[], groups: Dictionary<Group>) => {
    return selectedIds.map(id => groups[id]);
  }
);
