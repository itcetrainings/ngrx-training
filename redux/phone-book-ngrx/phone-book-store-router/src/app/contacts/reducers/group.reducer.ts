import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import {ContactDetailsPageActions, GroupActions, GroupsApiActions} from '../actions';
import {Group} from '../models/group.model';

export const groupsFeatureKey = 'groups';

export interface State extends EntityState<Group> {
  loading: boolean;
  loaded: boolean;
  selectedGroupIds: number[];
}

export const adapter: EntityAdapter<Group> = createEntityAdapter<Group>();

export const initialState: State = adapter.getInitialState({
  selectedGroupIds: [],
  loading: false,
  loaded: false
});

const groupReducer = createReducer(
  initialState,
  on(GroupActions.loadCollection, (state) => ({
    ...state,
    loading: true,
    loaded: false
  })
  ),
  on(GroupsApiActions.loadGroupsSuccess,
    (state, action) => adapter.addAll(action.groups, {
      ...state,
      loading: false,
      loaded: true
    })
  ),
  on(GroupsApiActions.loadGroupsFailure,
    state => adapter.removeAll(state)
  ),
  on(ContactDetailsPageActions.visualiseGroups,
    (state, action) => ({
        ...state,
      selectedGroupIds: action.ids
    })
  )
);

export function reducer(state: State | undefined, action: Action) {
  return groupReducer(state, action);
}

export const {
  selectAll,
  selectEntities
} = adapter.getSelectors();

export const getSelectedGroupIds = (state: State) => state.selectedGroupIds;

export const getLoading = (state: State) => state.loading;

export const getLoaded = (state: State) => state.loaded;

