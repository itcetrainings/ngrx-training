import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ContactsComponent} from './containers/contacts.component';
import {ContactDetailsComponent} from './containers/contact-details.component';

import {CanDeactivateGuard} from './guards/can-deactivate-guard';
import {ContactsGuard} from './guards/contacts.guard';
import {ContactExistsGuard} from './guards/contact-exists.guard';
import {GroupsGuard} from './guards/groups.guard';

const routes: Routes = [
  {
    path: 'contacts',
    component: ContactsComponent,
    canActivate: [ContactsGuard],
    children: [
      {path: ':id', component: ContactDetailsComponent, canDeactivate: [CanDeactivateGuard], canActivate: [ContactExistsGuard, GroupsGuard]},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard, ContactsGuard, ContactExistsGuard, GroupsGuard]
})
export class ContactsRoutingModule {
}
