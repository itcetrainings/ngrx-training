import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {EffectsModule} from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {StoreModule} from '@ngrx/store';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {reducers, metaReducers} from './reducers';

import { environment } from '../environments/environment';
import {ContactsModule} from './contacts/contacts.module';
import {AboutComponent} from './about/containers/about.component';
import {CustomSerializer} from './reducers/router/custom-serializer';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule, AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ContactsModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      name: 'Angular Training App',
      logOnly: environment.production
    }),
    StoreRouterConnectingModule.forRoot({
        serializer: CustomSerializer
      })]
  ,
  declarations: [
    AppComponent,
    AboutComponent
  ],
  bootstrap: [AppComponent],
  providers: []
})
export class AppModule {
}
