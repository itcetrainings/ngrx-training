import {
  ActionReducerMap,
  Action,
  MetaReducer, ActionReducer
} from '@ngrx/store';
import {InjectionToken} from '@angular/core';

import { environment } from '../../environments/environment';

export interface State {
}

//

export const reducers = new InjectionToken<
  ActionReducerMap<State, Action>
  >('Root reducers token', {
  factory: () => ({

  }),
});


// console.log all actions
export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return (state, action) => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();

    return result;
  };
}

export const metaReducers: MetaReducer<State>[] = !environment.production ? [logger] : [];
