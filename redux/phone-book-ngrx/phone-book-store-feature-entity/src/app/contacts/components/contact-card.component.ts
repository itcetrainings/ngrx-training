import {AfterContentChecked, Component, ContentChild} from '@angular/core';

import {GroupsListComponent} from './groups-list.component';

@Component({
  selector: 'contact-card',
  template: `
      <div class="contact-card">
          <ng-content>
          </ng-content>

          <ng-content select="groups-list">
          </ng-content>

          <div class="contact-card__indicator" *ngIf="selectedItems !== null">
              {{selectedItems}} groups selected
          </div>
      </div>
  `,
})
export class ContactCardComponent implements AfterContentChecked {
  @ContentChild(GroupsListComponent, {static: false}) groupsList: GroupsListComponent;

  public selectedItems: number;

  ngAfterContentChecked(): void {
    if (this.groupsList) {
      this.selectedItems = this.groupsList.value.length;
    } else {
      this.selectedItems = null;
    }
  }
}

