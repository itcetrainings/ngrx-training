import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

import {Contact} from '../models/contact.model';

@Component({
  selector: 'contacts-list',
  template: `
      <ul>
          <li *ngFor="let contact of contacts" class="item" [class.active]="selected==contact.id">
              <a (click)="onSelect(contact)">{{contact.firstName}} {{contact.lastName | uppercase}}</a>
              <a (click)="remove(contact)" class="remove" title="Remove"><span
                      class="glyphicon glyphicon-remove-sign"></span></a>
          </li>
      </ul>
  `
})
export class ContactsListComponent implements OnInit {
  @Input() contacts: Contact[];


  remove(contact: Contact) {
    //
  }

  onSelect(contact: Contact) {
    //
  }

  ngOnInit() {
    //
  }

}
