import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import {Contact} from '../models/contact.model';
import {ContactApiActions} from '../actions';

export const contactsFeatureKey = 'contacts';

export interface State extends EntityState<Contact> {
  loaded: boolean;
  loading: boolean;
}

export const adapter: EntityAdapter<Contact> = createEntityAdapter<Contact>({
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  loaded: false,
  loading: false
});

const contactReducer = createReducer(
  initialState,
  on(ContactApiActions.loadContactsSuccess,
    (state, action) => adapter.addAll(action.contacts, {
      ...state,
      loading: false,
      loaded: true
    })
  ),
  on(ContactApiActions.loadContactsFailure,
    (state, action) => adapter.removeAll( {
      ...state,
      loading: true,
      loaded: false
    })
  ),
  on(ContactApiActions.loadContactsFailure,
    (state, action) => adapter.removeAll( {
      ...state,
      loading: false,
      loaded: false
    })
  )
);

export function reducer(state: State | undefined, action: Action) {
  return contactReducer(state, action);
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();

export const getLoaded = (state: State) => state.loaded;

export const getLoading = (state: State) => state.loading;
