import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs';

import {ContactApiActions, ContactsPageActions} from '../actions';

import {ContactsService} from '../services/contact.service';
import { Contact } from '../models/contact.model';


@Injectable()
export class ContactsEffects {
  loadContacts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactsPageActions.loadCollection),
      switchMap(() =>
        this.contactsService.getAll().pipe(
          map((contacts: Contact[]) =>
            ContactApiActions.loadContactsSuccess({contacts})
          ),
          catchError(error =>
            of(ContactApiActions.loadContactsFailure({error}))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private contactsService: ContactsService
    ) {}
}
