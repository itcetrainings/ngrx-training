import { createAction, props } from '@ngrx/store';

import {Contact} from '../models/contact.model';


export const loadContacts = createAction(
  '[Contact] Load Contacts',
  props<{ contacts: Contact[] }>()
);
