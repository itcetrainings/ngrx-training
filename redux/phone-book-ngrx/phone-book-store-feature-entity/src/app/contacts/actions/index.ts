import * as ContactApiActions from './contact-api.actions';
import * as ContactsPageActions from './contacts-page.actions';

export {
  ContactApiActions,
  ContactsPageActions,
};
