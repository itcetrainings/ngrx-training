import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {CanDeactivateGuard} from './guards/can-deactivate-guard';

import {ContactsComponent} from './containers/contacts.component';
import {ContactDetailsComponent} from './containers/contact-details.component';

const routes: Routes = [
  {
    path: 'contacts',
    component: ContactsComponent,
    children: [
      {path: ':id', component: ContactDetailsComponent, canDeactivate: [CanDeactivateGuard]},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard]
})
export class ContactsRoutingModule {
}
