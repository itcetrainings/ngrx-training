import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';

import * as fromContacts from '../reducers';

import {Contact} from '../models/contact.model';
import {ContactsPageActions} from '../actions';


@Component({
  selector: 'contacts',
  template: `
      <div *ngIf="(loaded$ | async) === true">
          <contacts-list
            [selectedContact]="selectedContact$ | async"
            [contacts]="contacts$ | async"
            (contactRemove)="onContactRemove($event)">
          </contacts-list>

          <a id="add" class="text-danger" [routerLink]="['/contacts', -1]"><span class="glyphicon glyphicon-plus"></span>Add</a>

          <router-outlet></router-outlet>
      </div>
      <div *ngIf="(loading$ | async) === true">
          Loading...
      </div>
  `
})
export class ContactsComponent implements OnInit {
  contacts$: Observable<Contact[]>;
  selectedContact$: Observable<Contact>;
  loading$: Observable<boolean>;
  loaded$: Observable<boolean>;

  constructor(
    private store: Store<fromContacts.State>
  ) {}

  ngOnInit(): void {
    this.store.dispatch(ContactsPageActions.loadCollection());
    this.selectedContact$ = this.store.pipe(
      select(fromContacts.getSelectedContact)
    );

    this.contacts$ = this.store.pipe(
      select(fromContacts.getContactsCollection)
    );

    this.loading$ = this.store.pipe(
      select(fromContacts.getContactsLoading)
    );

    this.loaded$ = this.store.pipe(
      select(fromContacts.getContactsLoaded)
    );
  }

  onContactRemove(contact: Contact) {
    this.store.dispatch(ContactsPageActions.removeContact({contact}));
  }

}
