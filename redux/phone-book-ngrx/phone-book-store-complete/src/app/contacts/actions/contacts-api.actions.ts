import {createAction, props} from '@ngrx/store';

import {Contact} from '../models/contact.model';

export const addContactSuccess = createAction(
  '[Contacts API] Add Contact Success',
  props<{ contact: Contact }>()
);

export const addContactFailure = createAction(
  '[Contacts API] Add Contact Failure',
  props<{ contact: Contact }>()
);

export const updateContactSuccess = createAction(
  '[Contacts API] Update Contact Success',
  props<{ contact: Contact }>()
);

export const updateContactFailure = createAction(
  '[Contacts API] Update Contact Failure',
  props<{ contact: Contact }>()
);

export const removeContactSuccess = createAction(
  '[Contacts API] Remove Contact Success',
  props<{ contact: Contact }>()
);

export const removeContactFailure = createAction(
  '[Contacts API] Remove Contact Failure',
  props<{ contact: Contact }>()
);

export const loadContactsSuccess = createAction(
  '[Contacts API] Load Contacts Success',
  props<{ contacts: Contact[] }>()
);

export const loadContactsFailure = createAction(
  '[Contacts API] Load Contacts Failure',
  props<{ error: any }>()
);

