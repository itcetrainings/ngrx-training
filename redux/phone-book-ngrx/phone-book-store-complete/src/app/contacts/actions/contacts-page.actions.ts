import {createAction, props} from '@ngrx/store';

import {Contact} from '../models/contact.model';

export const loadCollection = createAction(
  '[Contacts Page] Load Collection'
);

export const selectContact = createAction(
  '[Contacts Page] Select Contact',
  props<{ id: number }>()
);

export const removeContact = createAction(
  '[Contacts Page] Remove Contact',
  props<{ contact: Contact }>()
);
