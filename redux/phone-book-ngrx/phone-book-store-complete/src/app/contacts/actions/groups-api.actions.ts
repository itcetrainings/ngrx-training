import { createAction, props } from '@ngrx/store';

import {Group} from '../models/group.model';

export const loadGroupsSuccess = createAction(
  '[Groups API] Load Groups Success',
  props<{ groups: Group[] }>()
);

export const loadGroupsFailure = createAction(
  '[Groups API] Load Groups Failure',
  props<{ groups: Group[] }>()
);
