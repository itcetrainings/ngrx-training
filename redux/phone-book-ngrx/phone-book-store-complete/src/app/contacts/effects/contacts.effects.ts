import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, mergeMap, switchMap} from 'rxjs/operators';
import {of} from 'rxjs';

import {ContactsApiActions, ContactActions, ContactDetailsPageActions, ContactsPageActions} from '../actions';
import {Contact} from '../models/contact.model';
import {ContactsService} from '../services/contact.service';

@Injectable()
export class ContactsEffects {
  loadCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactsPageActions.loadCollection),
      switchMap(() =>
        this.contactsService.getAll().pipe(
          map((contacts: Contact[]) =>
            ContactsApiActions.loadContactsSuccess({contacts})
          ),
          catchError(error =>
            of(ContactsApiActions.loadContactsFailure({error}))
          )
        )
      )
    )
  );

  removeContactFromCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactsPageActions.removeContact),
      mergeMap(({contact}) =>
        this.contactsService.remove(contact.id).pipe(
          map(() => ContactsApiActions.removeContactSuccess({contact})),
          catchError(() =>
            of(ContactsApiActions.removeContactFailure({contact}))
          )
        )
      )
    )
  );

  addContactToCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactDetailsPageActions.addContact),
      mergeMap(({contact}) =>
        this.contactsService.add(contact).pipe(
          mergeMap(newContact =>
            [
              ContactsApiActions.addContactSuccess({contact: newContact}),
              ContactActions.loadContact({contact: newContact})
            ]),
          catchError(() =>
            of(ContactsApiActions.addContactFailure({contact}))
          )
        )
      )
    )
  );

  updateContactInCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactDetailsPageActions.updateContact),
      mergeMap(({contact}) =>
        this.contactsService.update(contact).pipe(
          map(updatedContact => ContactsApiActions.updateContactSuccess({contact: updatedContact})),
          catchError(error =>
            of(ContactsApiActions.updateContactFailure(error))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private contactsService: ContactsService
  ) {
  }
}
