import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import {Contact} from '../models/contact.model';
import {ContactDetailsPageActions, ContactsApiActions} from '../actions';

export const contactsFeatureKey = 'contacts';

export interface State extends EntityState<Contact> {
  loaded: boolean;
  loading: boolean;
  selectedContactId: number;
}

export const adapter: EntityAdapter<Contact> = createEntityAdapter<Contact>({
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  loaded: false,
  loading: false,
  selectedContactId: null
});

const contactReducer = createReducer(
  initialState,
  on(ContactsApiActions.loadContactsSuccess,
    (state, action) => adapter.addAll(action.contacts, {
      ...state,
      loading: false,
      loaded: true
    })
  ),
  on(ContactsApiActions.loadContactsFailure,
    (state, action) => adapter.removeAll( {
      ...state,
      loading: true,
      loaded: false
    })
  ),
  on(ContactsApiActions.loadContactsFailure,
    (state, action) => adapter.removeAll( {
      ...state,
      loading: false,
      loaded: false
    })
  ),
  on(
    ContactsApiActions.updateContactSuccess,
    (state, {contact}) => adapter.updateOne({id: contact.id , changes: contact}, state)
  ),
  on(
    ContactDetailsPageActions.selectContact,
    (state,  {id}) => ({
      ...state,
      selectedContactId: id,
    })
  ),
  on(
    ContactsApiActions.addContactSuccess,
    ContactsApiActions.removeContactFailure,
    (state, {contact}) => adapter.addOne(contact, state)
  ),
  on(
    ContactsApiActions.removeContactSuccess,
    ContactsApiActions.addContactFailure,
    (state, {contact}) => adapter.removeOne(contact.id, state)
  )
);

export function reducer(state: State | undefined, action: Action) {
  return contactReducer(state, action);
}

export const {
  selectIds,
  selectEntities,
  selectAll
} = adapter.getSelectors();

export const getLoaded = (state: State) => state.loaded;

export const getLoading = (state: State) => state.loading;

export const getSelectedId =  (state: State) => state.selectedContactId;
