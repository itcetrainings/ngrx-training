import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import {ContactsRoutingModule} from './contacts-routing.module';
import {DialogService} from '../dialog.service';
import {ContactsComponent} from './containers/contacts.component';
import {ContactsListComponent} from './components/contacts-list.component';
import {ContactDetailsComponent} from './containers/contact-details.component';
import {ContactsService} from './services/contact.service';
import {GroupsListComponent} from './components/groups-list.component';
import {ContactCardComponent} from './components/contact-card.component';
import {ContactFormComponent} from './components/contact-form.component';

import {ContactsEffects} from './effects/contacts.effects';
import {GroupsEffects} from './effects/groups.effects';

import * as fromContacts from './reducers';
import {GroupsDisplayComponent} from './components/groups-display.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ContactsRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forFeature(fromContacts.contactsFeatureKey, fromContacts.reducers),
    EffectsModule.forFeature([ContactsEffects, GroupsEffects])
  ],
  declarations: [
    ContactsComponent,
    ContactsListComponent,
    ContactDetailsComponent,
    GroupsListComponent,
    GroupsDisplayComponent,
    ContactCardComponent,
    ContactFormComponent
  ],
  providers: [ContactsService, DialogService],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ContactsModule {
}
