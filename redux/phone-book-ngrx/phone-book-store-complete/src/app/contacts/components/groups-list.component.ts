import {ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

import {Group} from '../models/group.model';

const CONTACT_GROUPS_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => GroupsListComponent),
  multi: true,
};

@Component({
  selector: 'groups-list',
  providers: [CONTACT_GROUPS_ACCESSOR],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
        <span *ngFor="let group of availableGroups" class="label checkbox-inline label-default"
             (click)="selectGroup(group)"
             [class.label-success]="existsInGroups(group)">
            <span>{{group.name}}</span>
        </span>
  `
})
export class GroupsListComponent implements ControlValueAccessor {
  @Input() availableGroups: Group[] = [];

  value: Group[] = [];

  constructor(private changeDetectorRef: ChangeDetectorRef) {
  }


  private onTouch: Function;
  public onModelChange: Function;

  registerOnChange(fn: Function) {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: Function) {
    this.onTouch = fn;
  }

  writeValue(value: Group[]) {
    if (value === null) {
      value = [];
    }
    this.value = value;
    this.changeDetectorRef.markForCheck();
  }

  selectGroup(group: Group) {
    if (this.existsInGroups(group)) {
      this.value = this.value.filter(item => item.id !== group.id);
    } else {
      this.value = [...this.value, group];
    }
    this.onTouch();
    this.onModelChange(this.value);
  }

  existsInGroups(group: Group) {
    return this.value.some(val => val.id === group.id);
  }
}
