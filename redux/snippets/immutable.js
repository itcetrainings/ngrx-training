/**
 * Mutable by design
 */

 //Object
const character = {name: 'Gordon Freeman'}
character.role = 'Scientist';

console.log(character)


// Array
const names = ['Gordon Freeman', 'Wallace Breen'];
names.push('G-Man');

console.log(names)

/**
 * Immutable by design
 */

 // String
const name = 'Gordon Freeman';
const upperCaseName = name.toUpperCase();

console.log(upperCaseName, name);

/**
 * Immutable operations
 */

 //Object
 const character = {name: 'Gordon Freeman'};
 
 // const updatedChracter = {...character, { ole: 'Scientist'});
 const updatedChracter = Object.assign({}, character, {role: 'Scientist'});

 console.log(character);

 console.log(updatedChracter);

 //Array

const names = ['Gordon Freeman', 'Wallace Breen'];

const newNames =  [...names, 'G-Man'];

console.log(names);

console.log(newNames);