import {
  ActionReducerMap,
  Action,
  MetaReducer
} from '@ngrx/store';
import {InjectionToken} from '@angular/core';

import { environment } from '../../environments/environment';

export interface State {
}

//

export const reducers = new InjectionToken<
  ActionReducerMap<State, Action>
  >('Root reducers token', {
  factory: () => ({

  }),
});


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
