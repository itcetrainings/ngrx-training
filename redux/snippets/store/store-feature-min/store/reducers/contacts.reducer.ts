import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {createReducer, on} from '@ngrx/store';

import {Contact} from '../../models/contact.model';
import {CollectionApiActions} from '../actions';

export interface State extends EntityState<Contact> {
}

export const adapter: EntityAdapter<Contact> = createEntityAdapter<Contact>({
  sortComparer: false,
});

export const initialState: State = adapter.getInitialState();

export const contactsFeatureKey = 'contacts';

export const reducer = createReducer(
  initialState,
  on(
    CollectionApiActions.loadContactsSuccess,
    // @ts-ignore
    (state,  {contacts}) => adapter.addAll(contacts, state)
  )
);
