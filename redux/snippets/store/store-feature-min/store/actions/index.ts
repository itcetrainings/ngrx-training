import * as ContactActions from './contact.actions';
import * as CollectionApiActions from './collection-api.actions';
import * as ContactsPageActions from './contacts-page.actions';
import * as ContactDetailsPageActions from './contact-details-page.actions';

export {
  ContactActions,
  CollectionApiActions,
  ContactsPageActions,
  ContactDetailsPageActions
};
