import {createAction, props} from '@ngrx/store';

import { Contact } from '../../models/contact.model';

/**
 * Add
 */
export const addContact = createAction(
  '[Contact Details Page] Add Contact',
  props<{ contact: Contact }>()
);

/**
 * Update
 */
export const updateContact = createAction(
  '[Contact Details Page] Remove Contact',
  props<{ contact: Contact }>()
);

/**
 * Remove
 */
export const removeContact = createAction(
  '[Contact Details Page] Remove Contact',
  props<{ contact: Contact }>()
);



