import {createAction, props} from '@ngrx/store';

import { Contact } from '../../models/contact.model';

export const loadContact = createAction(
  '[Contact Exists Guard] Load Contact',
    props<{contact: Contact}>()
);
