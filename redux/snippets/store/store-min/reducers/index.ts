import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  Action,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromContacts from '../contacts/store/reducers/contacts.reducer';
import {InjectionToken} from '@angular/core';
import {contactsFeatureKey, ContactsState} from '../contacts/store/reducers';

export interface State {
  [fromContacts.contactsFeatureKey]: fromContacts.State;
}

//

export const reducers = new InjectionToken<
  ActionReducerMap<State, Action>
  >('Root reducers token', {
  factory: () => ({
    [fromContacts.contactsFeatureKey]: fromContacts.reducer,
  }),
});


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];


//

export const getContactsState = createFeatureSelector<State, ContactsState>(
  fromContacts.contactsFeatureKey
);

//

export const getContactEntitiesState = createSelector(
  getContactsState,
  state => state.contacts
);


export const getContactCollectionState = createSelector(
  getContactEntitiesState,
  state => state.data
);
