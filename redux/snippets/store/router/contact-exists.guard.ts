import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, filter, map, switchMap, take, tap} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';
import {Injectable} from '@angular/core';

import * as fromContacts from '../reducers/index';

import {ContactsService} from '../services/contact.service';
import {ContactActions, ContactsPageActions} from '../actions';

@Injectable()
export class ContactExistsGuard implements CanActivate {
  constructor(
    private store: Store<fromContacts.State>,
    private contactsService: ContactsService,
    private router: Router
  ) {
  }

  waitForCollectionToLoad(): Observable<boolean> {
    return this.store.pipe(
      select(fromContacts.getContactsLoaded),
      filter(loaded => loaded),
      take(1)
    );
  }

  hasContactInStore(id: number): Observable<boolean> {
    return this.store.pipe(
      select(fromContacts.getContactsDictionary),
      map(entities => !!entities[id]),
      take(1)
    );
  }

  hasContactInApi(id: number): Observable<boolean> {
    if (id === -1) {
      return of(true);
    }
    return this.contactsService.getById(id).pipe(
      map(contact => ContactActions.loadContact({id: contact.id})),
      tap(action => this.store.dispatch(action)),
      map(contact => !!contact),
      catchError(() => {
        this.router.navigate(['/404']);
        return of(false);
      })
    );
  }


  hasContact(id: number): Observable<boolean> {
    return this.hasContactInStore(id).pipe(
      switchMap(inStore => {
        if (inStore) {
          return of(inStore);
        }

        return this.hasContactInApi(id);
      })
    );
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.waitForCollectionToLoad().pipe(
      switchMap(() => this.hasContact(parseInt(route.params['id'], 10))
      ));
  }
}
