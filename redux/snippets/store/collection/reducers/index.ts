import {Action, combineReducers, createFeatureSelector, createSelector} from '@ngrx/store';

import {Contact} from '../../models/contact.model';

import * as fromRoot from '../../../reducers/index';
import * as fromContacts from './contacts.reducer';
import * as fromCollection from './collection.reducer';

export const contactsFeatureKey = 'contacts';

export interface ContactsState {
  [fromContacts.contactsFeatureKey]: fromContacts.State;
  [fromCollection.collectionFeatureKey]: fromCollection.State;
}

export interface State extends fromRoot.State {
  [contactsFeatureKey]: ContactsState;
}

export function reducers(state: ContactsState | undefined, action: Action) {
  return combineReducers({
    [fromContacts.contactsFeatureKey]: fromContacts.reducer,
    [fromCollection.collectionFeatureKey]: fromCollection.reducer
  })(state, action);
}

//

export const getContactsState = createFeatureSelector<State, ContactsState>(
  contactsFeatureKey
);


//

export const getContactEntitiesState = createSelector(
  getContactsState,
  state => state.contacts
);


export const {
  selectIds: getContactIds,
  selectEntities: getContactEntities,
  selectAll: getAllContacts,
  selectTotal: getTotalContacts,
} = fromContacts.adapter.getSelectors(getContactEntitiesState);

//

export const getCollectionState = createSelector(
  getContactsState,
  (state: ContactsState) => state.collection
);

export const getCollectionContactIds = createSelector(
  getCollectionState,
  fromCollection.getIds
);

export const getCollectionLoading = createSelector(
  getCollectionState,
  fromCollection.getLoading
);

export const getCollectionLoaded = createSelector(
  getCollectionState,
  fromCollection.getLoaded
);


export const getContactCollection = createSelector(
  getContactEntities,
  getCollectionContactIds,
  (entities, ids) => {
    return ids
      .map(id => entities[id])
      .filter((contact): contact is Contact => contact !== null);
  }
);
