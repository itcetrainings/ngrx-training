import {createReducer, on} from '@ngrx/store';

import {CollectionApiActions, ContactsPageActions} from '../actions';

export interface State {
  ids: number[];
  loading: boolean;
  loaded: boolean;
}

export const initialState: State = {
  ids: [],
  loading: false,
  loaded: false
};

export const collectionFeatureKey = 'collection';

export const reducer = createReducer(
  initialState,
  on(ContactsPageActions.loadCollection, state => ({
    ...state,
    loading: true,
    loaded: false
  })),
  // @ts-ignore
  on(CollectionApiActions.loadContactsSuccess,  (state, { contacts }) => ({
    loaded: true,
    loading: false,
    ids: contacts.map(contact => contact.id)
  }))
);


export const getLoaded = (state: State) => state.loaded;

export const getLoading = (state: State) => state.loading;

export const getIds = (state: State) => state.ids;
