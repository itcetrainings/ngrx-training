import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs';

import {CollectionApiActions, ContactsPageActions} from '../actions';

import {ContactsService} from '../services/contact.service';
import { Contact } from '../models/contact.model';


@Injectable()
export class ContactsEffects {
  loadContacts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ContactsPageActions.loadCollection),
      switchMap(() =>
        this.contactsService.getAll().pipe(
          map((contacts: Contact[]) =>
            CollectionApiActions.loadContactsSuccess({contacts})
          ),
          catchError(error =>
            of(CollectionApiActions.loadContactsFailure({error}))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private contactsService: ContactsService
    ) {}
}
