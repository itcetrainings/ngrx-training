// Initial state tree
interface State {
    [stateName:string]: any[]
}

const initialState: State = {
    stocks: []
}

// Action

interface Action {
    type: string,
    payload?: any;
}

const action = {
    type: 'ADD_STOCK',
    payload: {
        label: 'Amazon',
        name: 'AMZN'
    }
}

//Reducer
interface Reducer {
    (state: State, action: Action): State;
}

function reducer(state: State, action: Action): State {
    switch(action.type) {
        case 'ADD_STOCK': {
            const stock = action.payload;
            const stocks = [...state.stocks, stock];

            return {stocks}
        }
    }
}

// State
const state: State = {
    stocks: [{ label: 'Amazon',name: 'AMZN'}]
};