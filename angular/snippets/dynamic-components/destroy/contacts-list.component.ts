import {Component, Input, Output, EventEmitter} from '@angular/core';

import {Contact} from './contact';

@Component({
  selector: 'contacts-list',
  template: `    
      <h1>Contacts list</h1>
      <ul>
          <li *ngFor="let contact of contacts" class="item" [class.active]="selected && selected.id==contact.id">
              <a (click)="onSelect(contact)">{{contact.firstName}} {{contact.lastName | uppercase}}</a>
              <a (click)="remove(contact)" class="remove" title="Remove"><span class="glyphicon glyphicon-remove-sign"></span></a>
          </li>
      </ul>
  `
})
export class ContactsListComponent {
  @Input()
  selected: Contact;

  @Input()
  contacts: Contact[];

  @Output()
  selectedChange = new EventEmitter<Contact>();

  constructor() {
  }

  remove(contact: Contact) {
    if (this.selected && contact.id === this.selected.id) {
      this.selected = null;
      this.selectedChange.emit(this.selected);
    }
  }

  onSelect(contact: Contact) {
    this.selected = contact;
    this.selectedChange.emit(this.selected);
  }
}
