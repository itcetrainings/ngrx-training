import {AfterContentInit, Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {Contact} from './contact';
import {ContactsService} from './contact.service';
import {Subscription} from 'rxjs';

import {ContactsListComponent} from './contacts-list.component';

@Component({
  selector: 'app-root',
  template: `
      <div #entry></div>
  `
})
export class AppComponent implements OnInit, OnDestroy, AfterContentInit {
  @ViewChild('entry', {static: true, read: ViewContainerRef}) entry: ViewContainerRef;

  selected: Contact;
  contacts: Contact[];
  contactsSub: Subscription;

  constructor(
      private contactsService: ContactsService,
      private resolver: ComponentFactoryResolver
  ) {
  }

  ngAfterContentInit() {
    const contactListFactory = this.resolver.resolveComponentFactory<ContactsListComponent>(ContactsListComponent);
    this.entry.createComponent(contactListFactory);
  }

  ngOnInit() {
    this.fetchContacts();
  }

  ngOnDestroy() {
    this.contactsSub.unsubscribe();
  }

  fetchContacts() {
    this.contactsService.getAll().subscribe(contacts => {
      this.contacts = contacts as Contact[];
    });
  }

  onAdd() {
    this.selected = {id: null, firstName: '', lastName: '', email: ''};
  }
}
