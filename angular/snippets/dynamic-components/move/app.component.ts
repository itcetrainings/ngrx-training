import {
  AfterContentInit,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Contact} from './contact';
import {ContactsService} from './contact.service';
import {Subscription} from 'rxjs';

import {ContactsListComponent} from './contacts-list.component';
import {ContactDetailsComponent} from './contact-details.component';

@Component({
  selector: 'app-root',
  template: `
      <a class="text-danger" (click)="destroyDetails()">
          <span class="glyphicon glyphicon-trash"></span>
          Destroy Contact Details
      </a>
      <br />
      <a class="text-danger" (click)="moveDetails()">
          <span class="glyphicon glyphicon-edit"></span>
          Move Contact Details
      </a>
      <div #listEntry></div>
      <div #detailsEntry></div>
  `
})
export class AppComponent implements OnInit, OnDestroy, AfterContentInit {
  @ViewChild('listEntry', {static: true, read: ViewContainerRef}) listEntry: ViewContainerRef;
  @ViewChild('detailsEntry', {static: true, read: ViewContainerRef}) detailsEntry: ViewContainerRef;

  contactsSub: Subscription;
  contactListSelectedSub: Subscription;

  contactListComponent: ComponentRef<ContactsListComponent>;
  contactDetailsComponent: ComponentRef<ContactDetailsComponent>;
  contactDetailsComponent2: ComponentRef<ContactDetailsComponent>;

  constructor(
    private contactsService: ContactsService,
    private resolver: ComponentFactoryResolver
  ) {
  }

  ngAfterContentInit() {
    const contactListFactory = this.resolver.resolveComponentFactory<ContactsListComponent>(ContactsListComponent);
    this.contactListComponent = this.listEntry.createComponent(contactListFactory);

    const contactDetailsFactory = this.resolver.resolveComponentFactory<ContactDetailsComponent>(ContactDetailsComponent);

    this.contactDetailsComponent2 = this.detailsEntry.createComponent(contactDetailsFactory, 0);
    this.contactDetailsComponent2.instance.contact = {id: null, firstName: '', lastName: '', email: ''};
    this.contactDetailsComponent2.instance.showEdit = true;

    this.contactDetailsComponent = this.detailsEntry.createComponent(contactDetailsFactory, 1);


    this.contactListComponent.instance.selectedChange
      .subscribe(contact => {
        this.contactDetailsComponent.instance.contact = contact;
        this.contactDetailsComponent.changeDetectorRef.detectChanges();
      });
  }

  ngOnInit() {
    this.fetchContacts();
  }

  ngOnDestroy() {
    this.contactsSub.unsubscribe();
    this.contactListSelectedSub.unsubscribe();
  }

  fetchContacts() {
    this.contactsService.getAll().subscribe(contacts => {
      this.contactListComponent.instance.contacts = contacts as Contact[];
    });
  }

  onAdd() {
    this.contactDetailsComponent.instance.contact = {id: null, firstName: '', lastName: '', email: ''};
  }

  destroyDetails() {
    this.contactDetailsComponent.destroy();
  }

  moveDetails() {
    this.detailsEntry.move(this.contactDetailsComponent2.hostView, 1);
    this.detailsEntry.move(this.contactDetailsComponent.hostView, 0);
  }
}
