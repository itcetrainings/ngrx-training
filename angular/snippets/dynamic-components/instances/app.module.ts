import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {ContactsListComponent} from './contacts-list.component';
import {ContactDetailsComponent} from './contact-details.component';
import {ContactsService} from './contact.service';
import {GroupsService} from './groups.service';
import {ContactCardComponent} from './contact-card.component';
import {GroupsListComponent} from './groups-list.component';

@NgModule({
  imports: [BrowserModule, FormsModule, HttpClientModule, ReactiveFormsModule],
  declarations: [AppComponent, ContactsListComponent, ContactDetailsComponent,
    GroupsListComponent, ContactCardComponent],
  bootstrap: [AppComponent],
  providers: [ContactsService, GroupsService],
  entryComponents: [ContactsListComponent, ContactDetailsComponent]
})
export class AppModule {
}
