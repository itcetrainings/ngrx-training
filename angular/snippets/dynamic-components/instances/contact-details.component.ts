import {Component, Input, Output, OnChanges, EventEmitter, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {take} from 'rxjs/operators';

import {Contact} from './contact';
import {ContactsService} from './contact.service';
import {GroupsService} from './groups.service';
import {Group} from './group';

@Component({
  selector: 'contact-details',
  template: `
      <contact-card *ngIf="contact">
          <div id="contactsDetailsContainer">
              <ng-container *ngIf="!showEdit">
                  <label>First Name: </label><b>{{contact.firstName}}</b><br/>
                  <label>Last Name: </label><b>{{contact.lastName}}</b><br/>
                  <label>Email: </label><b>{{contact.email}}</b><br/>
                  <label>Groups: </label>
                  <span *ngFor="let group of contact.groups" class="label display-label inline-block label-success">
                      <span>{{group.name}}</span>
                  </span>
                  <div>
                      <a class="text-danger" (click)="showEdit=true">
                          <span class="glyphicon glyphicon-edit"></span>
                          Edit
                      </a>
                  </div>
              </ng-container>
              <form [formGroup]="contactForm" *ngIf="showEdit && availableGroups" novalidate>
                  <label for="firstName">First Name: </label>
                  <input id="firstName" name="firstName" formControlName="firstName" required><br/>
                  <div class="alert alert-danger" role="alert"
                       *ngIf="contactForm.controls.firstName && !contactForm.controls.firstName.pristine
                   && !contactForm.controls.firstName.valid">
                      First name is required
                  </div>

                  <label for="lastName">Last Name: </label>
                  <input id="lastName" name="lastName" formControlName="lastName" required><br/>
                  <div class="alert alert-danger" role="alert"
                       *ngIf="contactForm.controls.lastName && !contactForm.controls.lastName.pristine
                       && !contactForm.controls.lastName.valid">
                      Last name is required
                  </div>

                  <label for="email">E-mail: </label>
                  <input id="email" name="email" formControlName="email"><br/>
                  <div class="alert alert-danger" role="alert" *ngIf="contactForm.controls.email && !contactForm.controls.email.valid">Email
                      is
                      invalid
                  </div>
                  <div class="form-group">
                      <label>Groups: </label>
                      <groups-list formControlName="groups" [availableGroups]="availableGroups"></groups-list>
                  </div>
                  <input type="submit" class="btn btn-danger" (click)="onSubmit()" value="{{ !contact.id ? 'Add' : 'Save' }}"
                         [disabled]="contactForm.invalid || contactForm.pristine"/>
                  <a href="#" class="text-danger" (click)="onCancel()">Cancel</a>
              </form>
          </div>
      </contact-card>
  `,
  styles: ['.alert {margin-left: 104px;}']
})
export class ContactDetailsComponent implements OnChanges, OnInit {
  @Input()
  contact: Contact;
  @Output()
  contactChange = new EventEmitter<Contact | null | Observable<never>>();
  @Output()
  collectionChange = new EventEmitter<Contact | Observable<never>>();

  showEdit: boolean;
  contactForm: FormGroup;
  availableGroups: Group[];


  constructor(
    private contactsService: ContactsService,
    private groupsService: GroupsService,
    private formBuilder: FormBuilder
  ) {
    this.contactForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.email],
      groups: [[]]
    });
  }

  ngOnInit() {
    this.groupsService.getAll()
      .pipe(
        take(1)
      ).subscribe(groups => {
      this.availableGroups = groups as Group[];
    });
  }

  ngOnChanges(changes) {
    if (changes && changes.contact && changes.contact.currentValue !== changes.contact.previousValue) {
      this.showEdit = (this.contact && this.contact.id === null);
      this.contactForm.reset();
      this.contactForm.patchValue(this.contact);
    }
  }

  onSubmit() {
    if (!this.contactForm.valid) {
      return;
    }

    const dirtyContact: Contact = this.contactForm.value;
    dirtyContact.id = this.contact.id;

    if (this.contact.id === null) {
      this.contactsService.add(dirtyContact)
        .pipe(take(1))
        .subscribe(newContact => {
          this.resetFormAndEmit(newContact as Contact);
        });
    } else {
      this.contactsService.update(dirtyContact)
        .pipe(take(1))
        .subscribe(updatedContact => {
          this.resetFormAndEmit(updatedContact as Contact);
        });
    }
  }

  resetFormAndEmit(contact: Contact) {
    this.contactForm.reset();
    this.contactChange.emit(contact);
    this.collectionChange.emit(contact);

    this.contact = contact as Contact;
    this.showEdit = false;
  }

  onCancel() {
    this.showEdit = false;

    if (this.contact.id === null) {
      this.contact = null;
      this.contactForm.reset();
      this.contactChange.emit(this.contact);
    }
  }
}

