import {
  AfterContentInit,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Contact} from './contact';
import {ContactsService} from './contact.service';
import {Subscription} from 'rxjs';

import {ContactsListComponent} from './contacts-list.component';
import {ContactDetailsComponent} from './contact-details.component';

@Component({
  selector: 'app-root',
  template: `
      <div #listEntry></div>
      <div #detailsEntry></div>
  `
})
export class AppComponent implements OnInit, OnDestroy, AfterContentInit {
  @ViewChild('listEntry', {static: true, read: ViewContainerRef}) listEntry: ViewContainerRef;
  @ViewChild('detailsEntry', {static: true, read: ViewContainerRef}) detailsEntry: ViewContainerRef;

  contactsSub: Subscription;
  contactListSelectedSub: Subscription;

  contactListComponent: ComponentRef<ContactsListComponent>;
  contactDetailsComponent: ComponentRef<ContactDetailsComponent>;

  constructor(
    private contactsService: ContactsService,
    private resolver: ComponentFactoryResolver
  ) {
  }

  ngAfterContentInit() {
    const contactListFactory = this.resolver.resolveComponentFactory<ContactsListComponent>(ContactsListComponent);
    this.contactListComponent = this.listEntry.createComponent(contactListFactory);

    const contactDetailsFactory = this.resolver.resolveComponentFactory<ContactDetailsComponent>(ContactDetailsComponent);
    this.contactDetailsComponent = this.detailsEntry.createComponent(contactDetailsFactory);

    this.contactListComponent.instance.selectedChange
      .subscribe(contact => {
        this.contactDetailsComponent.instance.contact = contact;
      });
  }

  ngOnInit() {
    this.fetchContacts();
  }

  ngOnDestroy() {
    this.contactsSub.unsubscribe();
    this.contactListSelectedSub.unsubscribe();
  }

  fetchContacts() {
    this.contactsService.getAll().subscribe(contacts => {
      this.contactListComponent.instance.contacts = contacts as Contact[];
    });
  }

  onAdd() {
    this.contactDetailsComponent.instance.contact = {id: null, firstName: '', lastName: '', email: ''};
  }
}
