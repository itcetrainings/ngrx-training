import {Group} from './group';

export interface Contact {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  groups?: Group[];
}
