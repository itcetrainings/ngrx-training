import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    template: `
      <input #box (keyup.enter)="values=box.value" (blur)="values=box.value">
      <p>{{values}}</p>
  `
})
export class AppComponent {
    values = '';
}
