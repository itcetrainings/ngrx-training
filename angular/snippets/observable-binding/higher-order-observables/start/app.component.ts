import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {fromEvent, interval} from 'rxjs';

@Component({
  selector: 'app-root',
  template: `
      <button #action>Do action</button>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit{
  @ViewChild('action', {static: false}) button: ElementRef<HTMLButtonElement>;

  ngAfterViewInit() {
    const buttonClick$ = fromEvent(this.button.nativeElement, 'click').subscribe(event => {
      const interval$ = interval(1000).subscribe(num => {
        console.log(num);
      });
    });
  }
}
