import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {fromEvent, interval, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  template: `
      <button #action>Do action</button>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('action', {static: false}) button: ElementRef<HTMLButtonElement>;

  customMergeMap = function myMergeMap({source, operator}) {

    /** source - buttonClick$ observable */
    return new Observable(observer => {
      source.subscribe(outerValue => {

        /** operator — the interval$ observable */
        operator.project(outerValue).subscribe(innerValue => {
          observer.next(innerValue);
        });

      });
    });
  };

  customSwitchMap = function mySwitchMap({source, operator}) {
    let innerSubscription;
    // source - buttonClick$ observable
    return new Observable(observer => {
      source.subscribe(outerValue => {
        // operator — the interval$ observable

        // unsubscribe previous subscription to inner observable
        if (innerSubscription && innerSubscription.unsubscribe) {
          innerSubscription.unsubscribe();
        }
        innerSubscription = operator.project(outerValue).subscribe(innerValue => {
          observer.next(innerValue);
        });

      });
    });
  };

  ngAfterViewInit() {
    const interval$ = interval(1000);
    const buttonClick$ = fromEvent(this.button.nativeElement, 'click');

    const clicksInterval$ = buttonClick$.pipe(
      map(event => {
        return interval$;
      })
    );

    //
    clicksInterval$.subscribe(num => console.log(num));

    //
    clicksInterval$.pipe(
      this.customSwitchMap
    ).subscribe(num => console.log(num));
  }
}
