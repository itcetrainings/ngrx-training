import {AfterViewInit, Component, OnDestroy, ViewChild} from '@angular/core'
import {debounceTime} from "rxjs/operators";
import {NgForm} from "@angular/forms";
import {Subscription} from "rxjs";

@Component({
    selector: 'root',
    template: `
      <form #form="ngForm">
          <input type="text" name="firstName" [ngModel]="formValue.firstName"><br/>
          Typed: {{formValue.firstName}}
      </form>
  `
})

export class AppComponent implements AfterViewInit, OnDestroy {
    formValue = '';
    sub: Subscription;

    @ViewChild('form', {static: false}) form: NgForm;

     ngAfterViewInit() {
        this.form.valueChanges
            .pipe(
                debounceTime(1000)
            )
            .subscribe(newValue => this.formValue = newValue)
    }

     ngOnDestroy() {
        this.sub.unsubscribe()
    }
}
