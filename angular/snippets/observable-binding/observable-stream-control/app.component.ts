import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {fromEvent, merge, Observable} from "rxjs";
import {map, scan} from "rxjs/operators";

@Component({
  selector: 'app-root',
  template: `
      Counter: <span #counter id="counter">{{ $totals | async}}</span><br/>
      <br/>
      <button #add id="add" value="+1">+</button>
      <button #sub id="sub" value="-1">-</button>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  $totals: Observable<number>;
  @ViewChild('add', {static: false}) addBtn: ElementRef<HTMLButtonElement>;
  @ViewChild('sub', {static: false}) subBtn: ElementRef<HTMLButtonElement>;
  @ViewChild('counter', {static: false}) counter: ElementRef<HTMLSpanElement>;

  ngAfterViewInit() {
    const increment$ = fromEvent(this.addBtn.nativeElement, 'click');
    const decrement$ = fromEvent(this.subBtn.nativeElement, 'click');
    const clicks$ = merge(increment$, decrement$).pipe(map((event) => parseInt(event.target['value'] , 10)));

    this.$totals = clicks$.pipe(scan(
        (total, value) => {
          console.log("The total is", total);
          console.log("The new value is", value);
          return total + value;
        }, 0
    ));
  }
}
