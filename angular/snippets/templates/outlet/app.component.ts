import {AfterContentInit, Component, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
      <div>
          <ng-container #entry [ngTemplateOutlet]="temp2"></ng-container>

          <div>
              <ng-template #temp let-name let-location="location">
                  {{location}}: {{name}}
              </ng-template>
          </div>

          <div>
              <ng-template #temp2>
                  Plovdiv: Foo Barov
              </ng-template>
          </div>
      </div>`,
})
export class AppComponent implements AfterContentInit {
  @ViewChild('entry', {read: ViewContainerRef, static: true}) entry: ViewContainerRef;
  @ViewChild('temp', {read: TemplateRef, static: true}) tmpl: TemplateRef<any>;

  ngAfterContentInit(): void {
    this.entry.createEmbeddedView(this.tmpl, {
      location: 'Sofia',
      $implicit: 'John Doeff'
    });
  }
}
