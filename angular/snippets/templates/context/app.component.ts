import {AfterContentInit, Component, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<div>
      <ng-container #entry></ng-container>
      
      <ng-template #temp let-name let-location="location">
          {{location}}: {{name}}
      </ng-template>
  </div>`,
})
export class AppComponent implements AfterContentInit {
  @ViewChild('entry', {read: ViewContainerRef, static: true}) entry: ViewContainerRef;
  @ViewChild('temp', { read: TemplateRef, static: true}) tmpl: TemplateRef<any>;

  ngAfterContentInit(): void {
    this.entry.createEmbeddedView(this.tmpl, {
      location: 'Sofia',
      $implicit: 'John Doeff'
    });
  }
}
