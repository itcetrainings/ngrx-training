import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Observable} from 'rxjs';
import {take} from 'rxjs/operators';

import {Contact} from './contact';
import {ContactsService} from './contact.service';

@Component({
  selector: 'contacts-list',
  template: `
      <ul>
          <li *ngFor="let contact of contacts" class="item" [class.active]="selected && selected.id==contact.id">
              <a (click)="onSelect(contact)">{{contact.firstName}} {{contact.lastName | uppercase}}</a>
              <a (click)="remove(contact)" class="remove" title="Remove"><span class="glyphicon glyphicon-remove-sign"></span></a>
          </li>
      </ul>
  `
})
export class ContactsListComponent implements OnInit {
  @Input()
  selected: Contact;

  @Input() set collectionChange(contact) {
    this.fetchContacts();
  }

  @Output()
  selectedChange = new EventEmitter<Contact>();

  contacts: Contact[] | Observable<never>;

  constructor(private contactsService: ContactsService) {
  }

  remove(contact: Contact) {
    if (this.selected && contact.id === this.selected.id) {
      this.selected = null;
      this.selectedChange.emit(this.selected);
    }

    this.contactsService.remove(contact.id)
      .pipe(take(1))
      .subscribe(() => {
      this.fetchContacts();
    });
  }

  onSelect(contact: Contact) {
    this.selected = contact;
    this.selectedChange.emit(this.selected);
  }

  ngOnInit() {
    this.fetchContacts();
  }

  fetchContacts() {
    this.contactsService.getAll()
      .pipe(take(1))
      .subscribe(
        data => this.contacts = data,
        error => alert(error));
  }
}
