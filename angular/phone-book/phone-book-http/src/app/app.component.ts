import {Component, OnInit, ViewChild} from '@angular/core';
import {Contact} from './contact';
import {ContactsListComponent} from './contacts-list.component';

@Component({
  selector: 'app-root',
  template: `
      <contacts-list [(selected)]="selected"></contacts-list>

      <a id="add" href="#" class="text-danger" (click)="onAdd()"><span class="glyphicon glyphicon-plus"></span>Add</a>

      <contact-details [(contact)]="selected" (collectionChange)="contactsListComponent.collectionChange = $event"></contact-details>
  `
})
export class AppComponent {
  @ViewChild(ContactsListComponent, {static: false}) public contactsListComponent: ContactsListComponent;

  selected: Contact;

  onAdd() {
    this.selected = {id: null, firstName: '', lastName: '', email: ''};
  }
}
