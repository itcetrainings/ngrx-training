import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

import {environment} from '../../environments/environment';
import {Group} from './group';


@Injectable({
  providedIn: 'root'
})
export class GroupsService {
  private groupsUrl = `${environment.apiUrl}groups`;

  constructor(
    private http: HttpClient
  ) {}

  getAll() {
    return this.http
      .get<Group[]>(this.groupsUrl)
      .pipe(catchError((error: any) => throwError(error.json())));
  }
}
