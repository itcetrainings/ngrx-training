import {Injectable} from '@angular/core';
import {catchError, tap} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';

import {environment} from '../../environments/environment';

import {Contact} from './contact';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  private static _contactId = 1;
  private contactsUrl = `${environment.apiUrl}contacts`;

  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<Contact[]>(this.contactsUrl)
      .pipe(
        tap(data => {
          data.map((val) => {
            ContactsService._contactId = Math.max(val.id, ContactsService._contactId);
          });
          ContactsService._contactId++;
          return data;
        }),
        catchError(this.handleError)
      );
  }

  getById(id: number) {
    return this.http.get<Contact>(`${this.contactsUrl}/${id}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  remove(id: number) {
    return this.http.delete<Contact>(`${this.contactsUrl}/${id}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  update(contact: Contact) {
    return this.http.put<Contact>(`${this.contactsUrl}/${contact.id}`, contact)
      .pipe(
        catchError(this.handleError)
      );
  }

  add(contact: Contact) {
    contact.id = ContactsService._contactId;
    return this.http.post<Contact>(this.contactsUrl, contact)
      .pipe(
        catchError(this.handleError)
      );
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
