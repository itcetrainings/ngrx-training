import {Component, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';
import {take} from 'rxjs/operators';

import {CanComponentDeactivate} from '../can-deactivate-guard';
import {DialogService} from '../dialog.service';
import {ContactDetailsComponent} from './contact-details.component';

@Component({
  selector: 'contacts',
  template: `
      <contacts-list></contacts-list>

      <a id="add" class="text-danger" [routerLink]="['/contacts', -1]"><span class="glyphicon glyphicon-plus"></span>Add</a>

      <contact-details></contact-details>
  `
})
export class ContactsComponent implements CanComponentDeactivate {
  @ViewChild(ContactDetailsComponent, {static: false})
  private contactDetailsComponent: ContactDetailsComponent;

  constructor(private dialogService: DialogService) {
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (!this.contactDetailsComponent.contactForm.dirty) {
      return true;
    }
    const dialogResult = fromPromise(this.dialogService.confirm('Discard changes?'));

    dialogResult
      .pipe(take(1))
      .subscribe(
        (result) => {
          if (!result) {
            this.contactDetailsComponent.showEdit = true;
          }
        });

    return dialogResult;
  }
}
