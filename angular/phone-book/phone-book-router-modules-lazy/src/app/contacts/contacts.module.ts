import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {ContactsComponent} from './contacts.component';
import {ContactsListComponent} from './contacts-list.component';
import {ContactDetailsComponent} from './contact-details.component';
import {ContactsService} from './contact.service';
import {ContactsRoutingModule} from './contacts-routing.module';
import {DialogService} from '../dialog.service';

@NgModule({
  imports: [CommonModule, FormsModule, ContactsRoutingModule, ReactiveFormsModule, HttpClientModule],
  declarations: [ContactsComponent, ContactsListComponent, ContactDetailsComponent],
  providers: [ContactsService, DialogService]
})
export class ContactsModule {
}
