import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Params, Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {filter, map, take, takeUntil} from 'rxjs/operators';

import {Contact} from './contact';
import {ContactsService} from './contact.service';

@Component({
  selector: 'contacts-list',
  template: `
      <ul>
          <li *ngFor="let contact of contacts" class="item" [class.active]="selected==contact.id">
              <a (click)="onSelect(contact)">{{contact.firstName}} {{contact.lastName | uppercase}}</a>
              <a (click)="remove(contact)" class="remove" title="Remove"><span
                      class="glyphicon glyphicon-remove-sign"></span></a>
          </li>
      </ul>
  `
})
export class ContactsListComponent implements OnInit, OnDestroy {
  selected: number;
  contacts: Contact[] | Observable<never>;
  unsubscribe$ = new Subject<void>();

  constructor(
    private contactsService: ContactsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  remove(contact: Contact) {
    this.contactsService.remove(contact.id)
      .pipe(
        take(1),
      )
      .subscribe(() => {
        if (contact.id === this.selected) {
          this.router.navigate(['/contacts', ''])
            .then(() => {
              this.fetchContacts();
            });
        } else {
          this.fetchContacts();
        }
      });
  }

  onSelect(contact: Contact) {
    this.router.navigate(['/contacts', contact.id]);
  }

  ngOnInit() {
    this.contactsService.collectionChange
      .pipe(
        takeUntil(this.unsubscribe$)
      ).subscribe(() => {
      this.fetchContacts();
    });

    // Handle on first load
    if (this.route.firstChild) {
      this.selected = this.route.firstChild.snapshot.params['id'];
    }

    // Handle all subsequent route changes
    this.router.events.pipe(
      filter((e) => e instanceof NavigationEnd)
    ).subscribe(() => {
      if (this.route.firstChild) {
        this.route.firstChild
          .params.pipe(
          takeUntil(this.unsubscribe$),
          map((params: Params) => +params['id'])
        ).subscribe(contactId => this.selected = contactId);
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  fetchContacts() {
    this.contactsService.getAll()
      .pipe(
        take(1)
      ).subscribe(contacts => {
      this.contacts = contacts;
    });
  }
}
