import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {Contact} from './contact';
import {ContactsService} from './contact.service';

@Component({
  selector: 'contacts-list',
  template: `
      <ul>
          <li *ngFor="let contact of contacts" class="item" [class.active]="selected==contact">
              <a (click)="onSelect(contact)">{{contact.firstName}} {{contact.lastName | uppercase}}</a>
              <a (click)="remove(contact)" class="remove" title="Remove"><span class="glyphicon glyphicon-remove-sign"></span></a>
          </li>
      </ul>
  `
})
export class ContactsListComponent implements OnInit {
  @Input()
  selected: Contact;
  @Output()
  selectedChange = new EventEmitter<Contact>();

  contacts: Contact[];

  constructor(private contactsService: ContactsService) {
  }

  remove(person: Contact) {
    if (this.selected && person.id === this.selected.id) {
      this.selected = null;
      this.selectedChange.emit(this.selected);
    }

    this.contactsService.remove(person.id);
  }

  onSelect(person: Contact) {
    this.selected = person;
    this.selectedChange.emit(this.selected);
  }

  ngOnInit() {
    this.contacts = this.contactsService.getAll();
  }
}
