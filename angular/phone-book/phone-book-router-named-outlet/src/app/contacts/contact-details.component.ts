import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {take, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';

import {Contact} from './contact';
import {ContactsService} from './contact.service';
import {DialogService} from '../dialog.service';

@Component({
  selector: 'contact-details',
  template: `
      <div id="contactsDetailsContainer" *ngIf="contact">
            <span *ngIf="!showEdit">
                <label>First Name: </label><b>{{contact.firstName}}</b><br/>
                <label>Last Name: </label><b>{{contact.lastName}}</b><br/>
                <label>Email: </label><b>{{contact.email}}</b><br/>
                <label></label><a class="text-danger" (click)="showEdit=true"><span
                    class="glyphicon glyphicon-edit"></span>Edit</a><br/>
            </span>
          <form [formGroup]="contactForm" *ngIf="showEdit" novalidate>
              <label for="firstName">First Name: </label>
              <input id="firstName" name="firstName" formControlName="firstName" required><br/>
              <div class="alert alert-danger" role="alert"
                   *ngIf="contactForm.controls.firstName && !contactForm.controls.firstName.pristine &&
                   !contactForm.controls.firstName.valid">
                  First name is required
              </div>

              <label for="lastName">Last Name: </label>
              <input id="lastName" name="lastName" formControlName="lastName"
                     required><br/>
              <div class="alert alert-danger" role="alert"
                   *ngIf="contactForm.controls.lastName && !contactForm.controls.lastName.pristine && !contactForm.controls.lastName.valid">
                  Last name is required
              </div>

              <label for="email">Email: </label>
              <input id="email" name="email" formControlName="email"><br/>
              <div class="alert alert-danger" role="alert"
                   *ngIf="contactForm.controls.email && !contactForm.controls.email.valid">Email is invalid
              </div>


              <label></label>
              <input type="submit" class="btn btn-danger" (click)="onSubmit()"
                     value="{{ !contact.id ? 'Add' : 'Save' }}"
                     [disabled]="contactForm.invalid || contactForm.pristine"/>
              <a href="#" class="text-danger" (click)="onCancel()">Cancel</a>
          </form>
      </div>
  `,
  styles: ['.alert {margin-left: 104px;}']
})
export class ContactDetailsComponent implements OnInit, OnDestroy {
  contact: Contact;
  showEdit: boolean;
  contactForm: FormGroup;
  unsubscribe$ = new Subject<void>();

  constructor(
    private contactsService: ContactsService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: DialogService
  ) {
    this.contactForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.email]
    });
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((params) => {
        if (params['id']) {
          if (params['id'] !== '-1') {
            this.contactsService.getById(params['id']).subscribe(contact => {
              this.contact = contact as Contact;
              this.showEdit = false;
              this.resetForm();
            });
          } else {
            this.contact = {firstName: '', lastName: '', email: ''} as Contact;
            this.showEdit = true;
            this.resetForm();
          }
        } else {
          this.contact = null;
        }
      });
  }

  onSubmit() {
    if (!this.contactForm.valid) {
      return;
    }

    const contact: Contact = this.contactForm.value;
    contact.id = this.contact.id;

    if (!this.contact.id) {
      this.contactsService.add(contact)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(newContact => {
          this.resetFormAndNavigate(newContact as Contact);
        });
    } else {
      this.contactsService.update(contact)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(updatedContact => {
          this.resetFormAndNavigate(updatedContact as Contact);
        });
    }

    this.showEdit = false;
  }

  onCancel() {
    this.showEdit = false;
  }

  canDeactivate() {
    if (!this.contactForm.dirty) {
      return true;
    }
    const dialogResult = fromPromise(this.dialogService.confirm('Discard changes?'));

    dialogResult
      .pipe(take(1))
      .subscribe(
        (result) => {
          if (!result) {
            this.showEdit = true;
          }
        });

    return dialogResult;
  }

  resetFormAndNavigate(contact: Contact) {
    this.resetForm();
    this.contact = contact;
    this.router.navigate(['/contacts'])
      .then(() => {
        this.router.navigate(['/contacts', contact.id]);
      });
  }

  resetForm() {
    this.contactForm.reset({
      firstName: this.contact.firstName,
      lastName: this.contact.lastName,
      email: this.contact.email
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
